<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $table = "address";
    protected $fillable = [
        'apt_number',
        'street',
        'section',
        'postcode',
        'city',
        'state'       
    ];
}
