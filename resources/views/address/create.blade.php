<!DOCTYPE html>
<html lang="en">
<head>
  <title>Assignment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h4>Add new address</h4>
</div>
  
<div class="container">
<form method="post" action="{{ route('address.store') }}">
          @csrf
          <div class="form-group">    
              <label for="apt_number">Unit Number:</label>
              <input type="text" class="form-control" name="apt_number" placeholder="No."/>
          </div>

          <div class="form-group">
              <label for="street">Street:</label>
              <input type="text" class="form-control" name="street" placeholder="“Jalan ”, “Jln ”, “Lorong ”, “Persiaran”,"/>
          </div>

          <div class="form-group">
              <label for="section">Section:</label>
              <input type="text" class="form-control" name="section"/>
          </div>
          <div class="form-group">
              <label for="postcode">Postcode:</label>
              <input type="text" class="form-control" name="postcode"/>
          </div>
          <div class="form-group">
              <label for="city">City:</label>
              <select name="city" id="city">
                <option value="Kuala Terengganu">Kuala Terengganu</option>
                <option value="Kuala Lumpur">Kuala Lumpur</option>
                <option value="Kajang">Kajang</option>
                <option value="Bangi">Bangi</option>
                <option value="Damansara">Damansara</option>
                <option value="Petaling Jaya">Petaling Jaya</option>
                <option value="Puchong">Puchong</option>
                <option value="Subang Jaya">Subang Jaya</option>
                <option value="Cyberjaya">Cyberjaya</option>
                <option value="Putrajaya">Putrajaya</option>
                <option value="Mantin">Mantin</option>
                <option value="Kuching">Kuching</option>
                <option value="Seremban">Seremban</option>
              </select>
              <!-- <input type="text" class="form-control" name="city"/> -->
          </div>
          <div class="form-group">
              <label for="state">State:</label>
              <select name="city" id="city">
                <option value="Selangor">Selangor</option>
                <option value="Terengganu">Terengganu</option>
                <option value="Pahang">Pahang</option>
                <option value="Kelantan">Kelantan</option>
                <option value="Melaka">Melaka</option>
                <option value="Pulau Pinang">Pulau Pinang</option>
                <option value="Kedah">Kedah</option>
                <option value="Johor">Johor</option>
                <option value="Perlis">Perlis</option>
                <option value="Sabah">Sabah</option>
                <option value="Sarawak">Sarawak</option>
              </select>
              <!-- <input type="text" class="form-control" name="state"/> -->
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Submit</button>
      </form>
</div>

</body>
</html>
