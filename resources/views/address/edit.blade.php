<!DOCTYPE html>
<html lang="en">
<head>
  <title>Assignment</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h4>Update new address</h4>
</div>
  
<div class="container">
<form method="post" action="{{ route('address.update', $address->id) }}">
          @method('PUT')
          @csrf
          <div class="form-group">    
              <label for="apt_number">Unit Number:</label>
              <input type="text" class="form-control" name="apt_number" placeholder="No."value={{ $address->apt_number }} />
          </div>

          <div class="form-group">
              <label for="street">Street:</label>
              <input type="text" class="form-control" name="street" placeholder="“Jalan ”, “Jln ”, “Lorong ”, “Persiaran”," value={{ $address->street }}/>
          </div>

          <div class="form-group">
              <label for="section">Section:</label>
              <input type="text" class="form-control" name="section" value={{ $address->section }}/>
          </div>
          <div class="form-group">
              <label for="postcode">Postcode:</label>
              <input type="text" class="form-control" name="postcode" value={{ $address->postcode }}/>
          </div>
          <div class="form-group">
              <label for="city">City:</label>
              <select name="city" id="city" value={{ $address->city }}>
                <option value="kt">Kuala Terengganu</option>
                <option value="kl">Kuala Lumpur</option>
                <option value="kjg">Kajang</option>
                <option value="bng">Bangi</option>
                <option value="dam">Damansara</option>
                <option value="pj">Petaling Jaya</option>
                <option value="puc">Puchong</option>
                <option value="subj">Subang Jaya</option>
                <option value="cbj">Cyberjaya</option>
                <option value="puj">Putrajaya</option>
                <option value="mtn">Mantin</option>
                <option value="kuc">Kuching</option>
                <option value="ser">Seremban</option>
              </select>
              <!-- <input type="text" class="form-control" name="city"/> -->
          </div>
          <div class="form-group">
              <label for="state">State:</label>
              <select name="city" id="city" value={{ $address->state }}>
                <option value="Selangor">Selangor</option>
                <option value="Terengganu">Terengganu</option>
                <option value="Pahang">Pahang</option>
                <option value="Kelantan">Kelantan</option>
                <option value="Melaka">Melaka</option>
                <option value="Pulau Pinang">Pulau Pinang</option>
                <option value="Kedah">Kedah</option>
                <option value="Johor">Johor</option>
                <option value="Perlis">Perlis</option>
                <option value="Sabah">Sabah</option>
                <option value="Sarawak">Sarawak</option>
              </select>
              <!-- <input type="text" class="form-control" name="state"/> -->
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Update</button>
      </form>
</div>

</body>
</html>
