@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<div class="col-sm-12">
    <h1 class="display-3">Address</h1> 
    <div>
    <a style="margin: 19px;" href="{{ route('address.create')}}" class="btn btn-primary">New address</a>
    </div>  

  <table class="table table-striped">
    <thead>
        <tr>
          <td>No.</td>
          <td>Address</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($address as $address)
        <tr>
            <td>{{$address->id}}</td>
            <td>
            {{$address->apt_number ?? '-' }} 
            {{$address->street ?? '-' }} 
            {{$address->section ?? '-' }} 
            {{$address->postcode ?? '-' }} 
            {{$address->city ?? '-' }} 
            {{$address->state ?? '-' }}
            </td>
            <td>
                <a href="{{ route('address.edit',$address->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('address.destroy', $address->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection